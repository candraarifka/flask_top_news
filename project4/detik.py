import requests
from bs4 import BeautifulSoup
import time
import random

def getUrl():
	ua = ["Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36","Mozilla/5.0 (Macintosh; Intel Mac OS X x.y; rv:42.0) Gecko/20100101 Firefox/42.0"]
	headers={"User-Agent":random.choice(ua)}
	url="https://finance.detik.com/mostpopular?utm_source=wp&utm_medium=nav%20more&utm_campaign=most%20popular/mostpopular"
	r=requests.get(url,headers=headers)
	soup=BeautifulSoup(r.text,"html.parser")
	span=soup.find_all("article")
	gabung=[]
	for links in span:
		h2=links.find("h2").text
		link=links.find("a")["href"]
		gabung.append((h2,'\n'+link))
	#print(gabung[1])
	return gabung[0]
getUrl()
