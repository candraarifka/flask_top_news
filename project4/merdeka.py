import requests
from bs4 import BeautifulSoup
import time
import random

def getUrlm():
        ua = ["Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0","Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36","Mozilla/5.0 (Macintosh; Intel Mac OS X x.y; rv:42.0) Gecko/20100101 Firefox/42.0"]
        headers={"User-Agent":random.choice(ua)}
        url="https://www.merdeka.com/trending-article/"
        r=requests.get(url,headers=headers)
        soup=BeautifulSoup(r.text,"html.parser")
        span=soup.find_all("div",{"class":"mdk-trending-meta"})
        gabung=[]
        for links in span:
                title=links.find("a").text
                link=links.find("a")["href"]
                gabung.append((title,'\nhttps://www.merdeka.com'+link))
        return gabung[0]
getUrlm() 
