from flask import Flask, render_template, url_for, request, redirect
from flask_sqlalchemy import SQLAlchemy
from detik import getUrl
from merdeka import getUrlm

app = Flask(__name__)

getUrl=getUrl()

getUrlm=getUrlm()

@app.route('/', methods=['POST','GET'])
def layout():
	hasil = request.form.get("situs")
	if hasil != "":
		if hasil == "detik":
			hasil= getUrl
		elif hasil == "merdeka":
			hasil= getUrlm
		else:
			hasil="Masukkan hanya detik atau merdeka"
	return render_template('home.html', hasil=hasil)

@app.route('/about')
def about():
	return render_template('about.html')

@app.route('/contact')
def contact():
	return render_template('contact.html')

'''
@app.route('/tes')
def tes():
	return render_template('tes.html',hasil=getUrl)
'''
if __name__ == "__main__":
	app.run(debug=True)


	